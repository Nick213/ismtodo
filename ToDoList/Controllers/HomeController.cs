﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;

namespace ToDoList.Controllers
{
    public class HomeController : Controller
    {
        protected static ListItemContext db = new ListItemContext();

        public ActionResult Index()
        {
            if (db.ListItems.ToList().Count != 0)
            {
                db.ListItems.OrderBy(item => item.Date);
            }

            return View(db.ListItems.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ListItem item)
        {
            if (ModelState.IsValid)
            {
                db.ListItems.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ListItem item = db.ListItems.Find(id);

            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(ListItem item)
        {
            if (ModelState.IsValid)
            {
                db.ListItems.Remove(db.ListItems.Find(item.Id));
                db.ListItems.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ListItem item = db.ListItems.Find(id);

            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeletePost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var item = db.ListItems.Find(id);

            if (item == null)
            {
                return HttpNotFound();
            }

            db.ListItems.Remove(item);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Check(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ListItem li = db.ListItems.Find(id);

            if(li == null)
            {
                return HttpNotFound();
            }

            li.IsDone = !li.IsDone;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}