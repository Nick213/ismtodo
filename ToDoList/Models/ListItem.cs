﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class ListItem
    {
        [Key]
        public int Id { get; set; }
        public bool IsDone { get; set; } = false;
        
        [Required(ErrorMessage = "Введіть назву запису")]
        [MaxLength(20, ErrorMessage = "Занадто довга назва")]
        [Display(Name = "Назва")]
        public string Name { get; set; }

        [MaxLength(300, ErrorMessage = "Занадто довгий опис")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Опис")]
        public string Description { get; set; }

        [Display(Name = "Дата виконання")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public DateTime? Date { get; set; }
    }
}